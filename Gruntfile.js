module.exports = function (grunt) {

	require('time-grunt')(grunt);
	require('jit-grunt')(grunt);

	grunt.initConfig({
		clean: {
			all: {
				src: 'dist/*'
			},
			img: {
				src: 'dist/img/*'
			},
			newImg: {
				src: ['src/img/new/**/*', '!src/img/new/file.png']
			},
			js: {
				src: 'dist/js/*'
			},
			html: {
				src: 'dist/**/*.html'
			},
			css: {
				src: 'dist/css/*'
			}
		},
		jekyll: {
			dist: {
				options: {
					src : 'src/',
					dest: 'dist/',
					drafts: true,
					future: true,
					config: 'src/_config.yml'
				}
			}
		},
		connect: {
			server: {
				options: {
					base: 'dist/',
					port: 4000
				}
			}
		},
		imagemin: {
			newImg: {
				options: {
					optimizationLevel: 7,
					progressive: false
				},
				files: [
					{
						expand: true,
						cwd: 'src/img/new/',
						src: ['*.*', '!file.png'],
						dest: 'src/img'
					}
				]
			}
		},
		copy: {
			newImg: {
				expand: true,
				cwd: 'src/img',
				src: ['**/*.*', '!new/**'],
				dest: 'dist/img',
				// filter: 'isFile'
			}
		},
		compass: {
		    main: {
		        options: {
		            sassDir: 'src/sass',
		            cssDir: 'dist/css'
		        }
		    }
		},
		uglify: {
			dev: {
				options: {
					sourceMap: true,
					sourceMapIncludeSources: true,
					compress: false,
					beautify: true,
					mangleProperties: false
				},
				files: {
					'dist/js/external/jquery-2.1.4.min.js': 'src/js/external/jquery-2.1.4.min.js',
					'dist/js/main.min.js': [
						'src/js/external/fastclick.js',
						'src/js/main.js',
					]
				}
			}
		},
		watch: {
			options: {
				livereload: true,
				spawn: false
			},
			img: {
				files: ['src/img/new/**/*.*'],
				tasks: ['clean:img', 'images']
			},
			sass: {
				files: ['src/sass/**/*.{scss,css}'],
				tasks: ['clean:css', 'compass']
			},
			js: {
				files: ['src/js/**/*.js'],
				tasks: ['clean:js', 'uglify:dev']
			},
			jekyll: {
				files: ['src/**/*.html'],
				tasks: ['clean:html', 'jekyll', 'compass', 'uglify:dev']
			}
		}
	});

	/*
		1. clean dist/
		2. start server
	*/

	// SERVER TASKS
	grunt.registerTask('server', [
		'jekyll',
		'connect'
	]);

	// IMAGE TASKS
	grunt.registerTask('images', [
		'clean:img',
		// 'imagemin:newImg',
		'copy:newImg',
		'clean:newImg'
	]);

	grunt.registerTask('default', [
		'clean:all',
		'server',
		'images',
		'compass',
		'clean:js',
		'uglify:dev',
		'watch'
	]);

}